// Fill out your copyright notice in the Description page of Project Settings.

 
#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "ZmeykaBase.h"
#include "Components/InputComponent.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateZmeykaActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateZmeykaActor()
{
	ZmeykaActor = GetWorld()->SpawnActor<AZmeykaBase>(ZmeykaActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(ZmeykaActor))
	{
		if (value > 0 && ZmeykaActor->LastMoveDirection !=EMovementDeriction::DOWN)
		{
			ZmeykaActor->LastMoveDirection = EMovementDeriction::UP;
		}
		else if (value < 0 && ZmeykaActor->LastMoveDirection != EMovementDeriction::UP)
		{
			ZmeykaActor->LastMoveDirection = EMovementDeriction::DOWN;
		}
	}

}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(ZmeykaActor))
	{
		if (value > 0 && ZmeykaActor->LastMoveDirection != EMovementDeriction::LEFT)
		{
			ZmeykaActor->LastMoveDirection = EMovementDeriction::RIGHT;
		}
		else if (value < 0 && ZmeykaActor->LastMoveDirection != EMovementDeriction::RIGHT)
		{
			ZmeykaActor->LastMoveDirection = EMovementDeriction::LEFT;
		}
	}
}
