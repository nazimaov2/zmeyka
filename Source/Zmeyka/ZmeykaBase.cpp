//New
// Fill out your copyright notice in the Description page of Project Settings.


#include "ZmeykaBase.h"
#include "ZmeykaElementBase.h"
#include "Engine/World.h"
#include "Engine.h"
#include "Interactable.h"

// Sets default values
AZmeykaBase::AZmeykaBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDeriction::DOWN;
}

// Called when the game starts or when spawned
void AZmeykaBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddZmeykaElement(5);
}

// Called every frame
void AZmeykaBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void AZmeykaBase::AddZmeykaElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(ZmeykaElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		AZmeykaElementBase* NewZmeykaElem = GetWorld()->SpawnActor<AZmeykaElementBase>(ZmeykaElementClass, NewTransform);
		NewZmeykaElem->ZmeykaOwner = this;
		int32 ElemIndex = ZmeykaElements.Add(NewZmeykaElem);
	//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("ArrayIndex: %d"), ElemIndex));


		if (ElemIndex == 0)
		{
			NewZmeykaElem->SetFirstElementType();
			//MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AZmeykaElementBase::HandleBeginOverlap);


		}
	}
}
void AZmeykaBase::Move()
{
	FVector MovementVector(0);

	switch (LastMoveDirection)
	{
	case EMovementDeriction::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDeriction::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDeriction::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDeriction::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	//AddActorWorldOffset(MovementVector);
	ZmeykaElements[0]->ToggleCollision();
	for (int i = ZmeykaElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = ZmeykaElements[i];
		auto PrevElement = ZmeykaElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	ZmeykaElements[0]->AddActorWorldOffset(MovementVector);
	ZmeykaElements[0]->ToggleCollision();
}

void AZmeykaBase::ZmeykaElementOverlap(AZmeykaElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		ZmeykaElements.Find(OverlappedElement, ElemIndex);
		bool bisfirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bisfirst);

		}
	}
}
