// Fill out your copyright notice in the Description page of Project Settings.


#include "ZmeykaElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "ZmeykaBase.h"
// Sets default values
AZmeykaElementBase::AZmeykaElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	
}

// Called when the game starts or when spawned
void AZmeykaElementBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AZmeykaElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AZmeykaElementBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AZmeykaElementBase::HandleBeginOverlap);
}

void AZmeykaElementBase::Interact(AActor* Interactor, bool bisHead)
{
	auto Zmeyka = Cast<AZmeykaBase>(Interactor);
	if (IsValid(Zmeyka))
	{
		Zmeyka->Destroy();
	}

}

void AZmeykaElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	if (IsValid(ZmeykaOwner))
	{
		ZmeykaOwner->ZmeykaElementOverlap(this, OtherActor);
	}
}

void AZmeykaElementBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	
}

